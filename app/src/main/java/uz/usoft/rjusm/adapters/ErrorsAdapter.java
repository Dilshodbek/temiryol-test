package uz.usoft.rjusm.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import uz.usoft.rjusm.R;
import uz.usoft.rjusm.models.Question;


public class ErrorsAdapter extends RecyclerView.Adapter<ErrorsAdapter.MyViewHolder> {
    private Context context;
    private List<Question> errorQuestions;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            this.textView = itemView.findViewById(R.id.textError);
        }
    }

    public ErrorsAdapter(Context context, List<Question> errorQuestions) {
        this.context = context;
        this.errorQuestions = errorQuestions;
    }

    @NonNull
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MyViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.error_item, viewGroup, false));
    }

    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.textView.setText(""+(i+1)+"."+(errorQuestions.get(i)).getQUESTION());
    }

    public int getItemCount() {
        return this.errorQuestions.size();
    }
}

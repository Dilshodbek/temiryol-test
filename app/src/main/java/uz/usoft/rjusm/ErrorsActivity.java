package uz.usoft.rjusm;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.List;
import uz.usoft.rjusm.adapters.ErrorsAdapter;
import uz.usoft.rjusm.models.ErrorQuestion;
import uz.usoft.rjusm.models.Question;

public class ErrorsActivity extends AppCompatActivity {
    private List<ErrorQuestion> errorQuestionList;
    private ErrorsAdapter errorsAdapter;
    private List<Question> errorsQuestionList;
    private RecyclerView recyclerView;

    /* renamed from: uz.usoft.rjusm.uz.ErrorsActivity$1 */
    class C04671 extends TypeToken<List<Question>> {
        C04671() {
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_errors);
        this.recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        this.errorQuestionList = new ArrayList<>();
        this.errorsQuestionList = new ArrayList<>();
        this.errorsQuestionList = (List) new Gson().fromJson(getIntent().getExtras().getString("errors"), new C04671().getType());
        this.errorsAdapter = new ErrorsAdapter(this, this.errorsQuestionList);
        this.recyclerView.setAdapter(this.errorsAdapter);
    }
}

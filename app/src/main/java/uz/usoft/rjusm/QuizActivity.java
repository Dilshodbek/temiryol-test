package uz.usoft.rjusm;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.LightingColorFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.button.MaterialButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import uz.usoft.rjusm.data.MyData;
import uz.usoft.rjusm.models.Question;

public class QuizActivity extends AppCompatActivity {
    private RadioButton answer;
    private MaterialButton butNext;
    private CountDownTimer countDownTimer;
    private Question currentQ;
    private DatabaseAccess databaseAccess;
    private ArrayList errorList;
    private int langCode;
    private MyData myData;
    private int option;
    private int qid = 0;
    private List<Question> quesList;
    private List<Question> questionList;
    private RadioGroup radioGroup;
    private RadioButton rda;
    private RadioButton rdb;
    private RadioButton rdc;
    private RadioButton rdd;
    private int score = 0;
    private TextView timerText;
    private TextView txtQuestion;
    private TextView typeHeader;
    private TextView leftCounterTxt;

    /* renamed from: uz.usoft.rjusm.uz.QuizActivity$1 */
    class C03251 implements OnClickListener {
        C03251() {
        }

        public void onClick(View v) {
            QuizActivity quizActivity = QuizActivity.this;
            quizActivity.radioGroup = (RadioGroup) quizActivity.findViewById(R.id.rdGroup);
            quizActivity = QuizActivity.this;
            quizActivity.answer = (RadioButton) quizActivity.findViewById(quizActivity.radioGroup.getCheckedRadioButtonId());
            QuizActivity.this.radioGroup.clearCheck();
            if (QuizActivity.this.answer != null) {
                if (QuizActivity.this.currentQ.getANSWER().equals(QuizActivity.this.answer.getText())) {
                    QuizActivity.this.score = QuizActivity.this.score + 1;
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("Your score");
                    stringBuilder.append(QuizActivity.this.score);
                    Log.d("score", stringBuilder.toString());
                } else {
                    QuizActivity.this.errorList.add(QuizActivity.this.currentQ);
                }
                if (QuizActivity.this.qid <= QuizActivity.this.questionList.size() - 1) {
                    quizActivity = QuizActivity.this;
                    quizActivity.currentQ = (Question) quizActivity.quesList.get(QuizActivity.this.qid);
                    QuizActivity.this.setQuestionView();
                    return;
                }
                QuizActivity.this.finishTest();
                return;
            }
            Context context = QuizActivity.this;
            Toast.makeText(context, langCode == 0 ? R.string.please_choose_uz : R.string.please_choose_ru, Toast.LENGTH_SHORT).show();
        }
    }

    /* renamed from: uz.usoft.rjusm.uz.QuizActivity$2 */
    class C03262 implements DialogInterface.OnClickListener {
        C03262() {
        }

        public void onClick(DialogInterface dialog, int which) {
            QuizActivity.this.databaseAccess.close();
            QuizActivity.this.finish();
        }
    }

    /* renamed from: uz.usoft.rjusm.uz.QuizActivity$3 */
    class C03273 implements DialogInterface.OnClickListener {
        C03273() {
        }

        public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_quiz);
        this.butNext = (MaterialButton) findViewById(R.id.materialButton10);
        this.typeHeader = (TextView) findViewById(R.id.typeHeader);
        leftCounterTxt=findViewById(R.id.leftCounterTxt);
        this.langCode = getIntent().getIntExtra("lang", 0);
        this.option = getIntent().getIntExtra("table", 0);
        this.typeHeader.setText(changeHeaderTextUz(this.option, this.langCode));
        this.errorList = new ArrayList<>();
        this.databaseAccess = DatabaseAccess.getInstance(this);
        this.databaseAccess.open();
        this.timerText = (TextView) findViewById(R.id.timerText);
        this.questionList = new ArrayList<>();
        this.quesList = this.databaseAccess.getQuestions(this.langCode, this.option);
        Collections.shuffle(this.quesList);
        if (this.quesList.size() >= 30) {
            for (int i = 0; i < 30; i++) {
                this.questionList.add(this.quesList.get(i));
            }
        } else {
            this.questionList.addAll(this.quesList);
        }
        this.currentQ = (Question) this.questionList.get(this.qid);
        this.txtQuestion = (TextView) findViewById(R.id.textView4);
        this.rda = (RadioButton) findViewById(R.id.rda);
        this.rdb = (RadioButton) findViewById(R.id.rdb);
        this.rdc = (RadioButton) findViewById(R.id.rdc);
        this.rdd = (RadioButton) findViewById(R.id.rdd);


        setQuestionView();
        setTimer();
        this.butNext.setOnClickListener(new C03251());
    }

    private void setQuestionView() {
        this.txtQuestion.setText(this.currentQ.getQUESTION());
        this.rda.setText(this.currentQ.getOPTA());
        this.rdb.setText(this.currentQ.getOPTB());
        this.rdc.setText(this.currentQ.getOPTC());
        this.rdd.setText(this.currentQ.getOPTD());
        this.qid++;

        leftCounterTxt.setText(String.format(Locale.getDefault(),"%d/30",qid));

    }

    public void onBackPressed() {
        setAlert();
    }

    public void setAlert() {
        String message;
        int i;
        Builder builder = new Builder(this);
        if (this.langCode == 0) {
            message = getString(R.string.cancel_test_alert_uz);
        } else {
            message = getString(R.string.cancel_test_alert_ru);
        }
        builder.setMessage(Html.fromHtml(message));
        builder.setPositiveButton((CharSequence) "OK", new C03262());
        builder.setNegativeButton(this.langCode == 0 ? "BEKOR QILISH" : "Отменить", new C03273());
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        Button negative = alertDialog.getButton(-2);
        Button positive = alertDialog.getButton(-1);
        negative.setTextColor(ContextCompat.getColor(this, R.color.yello_light));
        positive.setTextColor(ContextCompat.getColor(this, R.color.yello_light));
        alertDialog.getWindow().getDecorView().getBackground().setColorFilter(new LightingColorFilter(ViewCompat.MEASURED_STATE_MASK, ViewCompat.MEASURED_SIZE_MASK));
    }

    private String changeHeaderTextUz(int option, int langCode) {
        if (langCode == 0) {
            this.butNext.setText(R.string.next_uz);
            switch (option) {
                case 0:
                    return getResources().getString(R.string.type_1_uz);
                case 1:
                    return getResources().getString(R.string.type_2_uz);
                case 2:
                    return getResources().getString(R.string.type_3_uz);
                case 3:
                    return getResources().getString(R.string.type_4_uz);
                case 4:
                    return getResources().getString(R.string.type_5_uz);
                case 5:
                    return getResources().getString(R.string.type_6_uz);
                case 6:
                    return getResources().getString(R.string.type_7_uz);
                case 7:
                    return getResources().getString(R.string.type_8_uz);
                    case 8:
                    return getResources().getString(R.string.type_9_uz);
                case 9:
                    return getResources().getString(R.string.type_10_uz);
                default:
                    return "";
            }
        }
        this.butNext.setText(R.string.next_ru);
        switch (option) {
            case 0:
                return getResources().getString(R.string.type_1_ru);
            case 1:
                return getResources().getString(R.string.type_2_ru);
            case 2:
                return getResources().getString(R.string.type_3_ru);
            case 3:
                return getResources().getString(R.string.type_4_ru);
            case 4:
                return getResources().getString(R.string.type_5_ru);
            case 5:
                return getResources().getString(R.string.type_6_ru);
            case 6:
                return getResources().getString(R.string.type_7_ru);
            case 7:
                return getResources().getString(R.string.type_8_ru);
            case 8:
                return getResources().getString(R.string.type_9_ru);
                case 9:
                return getResources().getString(R.string.type_10_ru);
            default:
                return "";
        }
    }

    private void setTimer() {
        this.countDownTimer = new CountDownTimer(1200000, 1000) {
            public void onTick(long millisUntilFinished) {
                int minutes = ((int) (millisUntilFinished / 1000)) / 60;
                int seconds = ((int) (millisUntilFinished / 1000)) % 60;
                QuizActivity.this.timerText.setText(String.format("%02d:%02d", Integer.valueOf(minutes), Integer.valueOf(seconds)));
            }

            public void onFinish() {
                QuizActivity.this.timerText.setText(QuizActivity.this.langCode == 0 ? "Vaqt tugadi" : "Время прошло");
                QuizActivity.this.finishTest();
            }
        }.start();
    }

    private void finishTest() {
        Intent intent = new Intent(this, ResultsActivity.class);
        Bundle b = new Bundle();
        b.putInt("score", this.score);
        b.putInt("question_size", this.questionList.size());
        b.putInt("table", this.option);
        b.putInt("lang", this.langCode);
        b.putString("errors", new Gson().toJson(this.errorList));
        intent.putExtras(b);
        this.databaseAccess.close();
        startActivity(intent);
        finish();
    }
}

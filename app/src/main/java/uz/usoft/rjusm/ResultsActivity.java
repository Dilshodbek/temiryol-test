package uz.usoft.rjusm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.button.MaterialButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import uz.usoft.rjusm.models.Question;

public class ResultsActivity extends AppCompatActivity {
    private MaterialButton changeTypeBtn;
    private List errorsQuestionList;
    private String errorsSrting;
    private int langCode;
    private MaterialButton materialButtonErrors;
    private int option;
    private int questionSize;
    private MaterialButton restartBtn;
    private int score;
    private TextView scoreText;
    private TextView yourResultTxt;

    /* renamed from: uz.usoft.rjusm.uz.ResultsActivity$2 */
    class C03302 implements OnClickListener {
        C03302() {
        }

        public void onClick(View v) {
            Intent intent = new Intent(ResultsActivity.this, QuizActivity.class);
            Bundle b = new Bundle();
            b.putInt("table", ResultsActivity.this.option);
            b.putInt("lang", ResultsActivity.this.langCode);
            intent.putExtras(b);
            ResultsActivity.this.startActivity(intent);
            ResultsActivity.this.finish();
        }
    }

    /* renamed from: uz.usoft.rjusm.uz.ResultsActivity$3 */
    class C03313 implements OnClickListener {
        C03313() {
        }

        public void onClick(View v) {
            ResultsActivity.this.finish();
        }
    }

    /* renamed from: uz.usoft.rjusm.uz.ResultsActivity$4 */
    class C03324 implements OnClickListener {
        C03324() {
        }

        public void onClick(View v) {
            if (ResultsActivity.this.errorsQuestionList.size() == 0) {
                Context context = ResultsActivity.this;
                Toast.makeText(context, langCode == 0 ? "Xatolar mavjud emas" : "Нет ошибок", Toast.LENGTH_LONG).show();
                return;
            }
            Intent intent = new Intent(ResultsActivity.this, ErrorsActivity.class);
            intent.putExtra("errors", ResultsActivity.this.errorsSrting);
            ResultsActivity.this.startActivity(intent);
        }
    }

    /* renamed from: uz.usoft.rjusm.uz.ResultsActivity$1 */
    class C04681 extends TypeToken<List<Question>> {
        C04681() {
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_results);
        this.score = getIntent().getIntExtra("score", 0);
        this.questionSize = getIntent().getIntExtra("question_size", 0);
        this.option = getIntent().getIntExtra("table", 0);
        this.langCode = getIntent().getIntExtra("lang", 0);
        this.errorsSrting = getIntent().getStringExtra("errors");
        this.yourResultTxt = (TextView) findViewById(R.id.your_result_Txt);
        this.errorsQuestionList = (List) new Gson().fromJson(this.errorsSrting, new C04681().getType());
        this.scoreText = (TextView) findViewById(R.id.resultText);
        this.yourResultTxt.setText(this.langCode == 0 ? R.string.your_results_uz : R.string.your_results_ru);
        TextView textView = this.scoreText;
        String stringBuilder = String.valueOf(this.score) +
                "/" +
                String.valueOf(this.questionSize);
        textView.setText(stringBuilder);
        this.restartBtn = (MaterialButton) findViewById(R.id.restartBtn);
        this.changeTypeBtn = (MaterialButton) findViewById(R.id.changeTypeBtn);
        this.restartBtn.setOnClickListener(new C03302());
        this.changeTypeBtn.setOnClickListener(new C03313());
        this.materialButtonErrors = (MaterialButton) findViewById(R.id.showErrorsBtn);
        if (this.langCode == 0) {
            this.restartBtn.setText(R.string.start_again_uz);
            this.changeTypeBtn.setText(R.string.choose_another_type_uz);
            this.materialButtonErrors.setText(R.string.show_errors_uz);
        } else {
            this.restartBtn.setText(R.string.start_again_ru);
            this.changeTypeBtn.setText(R.string.choose_another_type_ru);
            this.materialButtonErrors.setText(R.string.show_errors_ru);
        }
        this.materialButtonErrors.setOnClickListener(new C03324());
    }
}

package uz.usoft.rjusm;

import android.content.Intent;
import android.support.design.button.MaterialButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class FirstChooserActivity extends AppCompatActivity {


    private MaterialButton btnType1,btnType2;
    private int langOpt = 1;
    private int option=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_chooser);

        btnType1=findViewById(R.id.materialButtonType1);
        btnType2=findViewById(R.id.materialButtonType2);




        btnType1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                option=10;
                startQuiz();
            }
        });


        btnType2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                option=11;
                startQuiz();
            }
        });


    }



    private void startQuiz() {
        Intent intent = new Intent(this, QuizActivity.class);
        intent.putExtra("lang", langOpt);
        intent.putExtra("table", option);
        startActivity(intent);
        finish();
    }
}

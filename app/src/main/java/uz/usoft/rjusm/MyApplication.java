package uz.usoft.rjusm;

import android.app.Application;
import android.content.Context;

public class MyApplication extends Application {
    public void onCreate() {
        super.onCreate();
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }
}

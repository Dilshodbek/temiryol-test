package uz.usoft.rjusm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;

public class LauncherActivity extends AppCompatActivity {
    private CountDownTimer countDownTimer;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_launcher);
        this.countDownTimer = new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                Context context = LauncherActivity.this;
                context.startActivity(new Intent(context, MainActivity.class));
                LauncherActivity.this.finish();
            }
        }.start();
    }
}

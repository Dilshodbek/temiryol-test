package uz.usoft.rjusm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.button.MaterialButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;

public class PauseActivity extends AppCompatActivity {
    private MaterialButton materialButton;

    /* renamed from: uz.usoft.rjusm.uz.PauseActivity$1 */
    class C03241 implements OnClickListener {
        C03241() {
        }

        public void onClick(View v) {
            Context context = PauseActivity.this;
            context.startActivity(new Intent(context, ResultsActivity.class));
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_pause);
        this.materialButton = (MaterialButton) findViewById(R.id.materialButton12);
        this.materialButton.setOnClickListener(new C03241());
    }
}

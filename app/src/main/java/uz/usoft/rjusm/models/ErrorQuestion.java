package uz.usoft.rjusm.models;

public class ErrorQuestion {
    private int id;
    private String text;

    public ErrorQuestion(int id, String text) {
        this.id = id;
        this.text = text;
    }

    public int getId() {
        return this.id;
    }

    public String getText() {
        return this.text;
    }
}

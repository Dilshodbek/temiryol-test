package uz.usoft.rjusm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.button.MaterialButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements OnClickListener {
    private TextView bottom1txt;
    private TextView bottom2Txt;
    private TextView header1Txt;
    private TextView headerTxt2;
    private int langOpt = 0;
    private MaterialButton langRuBtn;
    private MaterialButton langUzBtn;
    private MaterialButton materialButton1;
    private MaterialButton materialButton2;
    private MaterialButton materialButton3;
    private MaterialButton materialButton4;
    private MaterialButton materialButton5;
    private MaterialButton materialButton6;
    private MaterialButton materialButton7;
    private MaterialButton materialButton8;
    private MaterialButton materialButton9;
    private MaterialButton materialButton10;
    private int option;
    private TextView questionMark;

    /* renamed from: uz.usoft.rjusm.uz.MainActivity$1 */
    class C03221 implements OnClickListener {
        C03221() {
        }

        public void onClick(View v) {
            MainActivity.this.langOpt = 0;
            MainActivity mainActivity = MainActivity.this;
            mainActivity.changeLanguageButtonColors(mainActivity.langOpt);
        }
    }

    /* renamed from: uz.usoft.rjusm.uz.MainActivity$2 */
    class C03232 implements OnClickListener {
        C03232() {
        }

        public void onClick(View v) {
            MainActivity.this.langOpt = 1;
            MainActivity mainActivity = MainActivity.this;
            mainActivity.changeLanguageButtonColors(mainActivity.langOpt);
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        materialButton1 = findViewById(R.id.materialButton1);
       materialButton2 = findViewById(R.id.materialButton2);
        materialButton3 =  findViewById(R.id.materialButton3);
      materialButton4 = findViewById(R.id.materialButton4);
        materialButton5 =  findViewById(R.id.materialButton5);
        materialButton6 =  findViewById(R.id.materialButton6);
        materialButton7 =  findViewById(R.id.materialButton7);
        materialButton8 = findViewById(R.id.materialButton8);
        materialButton9=findViewById(R.id.materialButton9);
        materialButton10=findViewById(R.id.materialButton10);
       questionMark =  findViewById(R.id.questionMark);
       header1Txt = findViewById(R.id.headet_1_Txt);
        headerTxt2 =  findViewById(R.id.header_2_Txt);
        bottom1txt =findViewById(R.id.bottom_1_Txt);
     bottom2Txt = findViewById(R.id.bottom_2_Txt);
       langUzBtn =  findViewById(R.id.lanUzBtn);
        langRuBtn = findViewById(R.id.lanRuBtn);
        changeLanguageButtonColors(this.langOpt);
        langUzBtn.setOnClickListener(new C03221());
        langRuBtn.setOnClickListener(new C03232());
        materialButton1.setOnClickListener(this);
        materialButton2.setOnClickListener(this);
        materialButton3.setOnClickListener(this);
        materialButton4.setOnClickListener(this);
        materialButton5.setOnClickListener(this);
        materialButton6.setOnClickListener(this);
        materialButton7.setOnClickListener(this);
      materialButton8.setOnClickListener(this);
      materialButton9.setOnClickListener(this);
      materialButton10.setOnClickListener(this);
    }

    public void onClick(View v) {
        int id = v.getId();
        if (id != R.id.materialButton1) {
            switch (id) {
                case R.id.materialButton2:
                    this.option = 1;
                    startQuiz();
                    return;
                case R.id.materialButton3:
                    this.option = 2;
                    startQuiz();
                    return;
                case R.id.materialButton4:
                    this.option = 3;
                    startQuiz();
                    return;
                case R.id.materialButton5:
                    this.option = 4;
                    startQuiz();
                    return;
                case R.id.materialButton6:
                    this.option = 5;
                    startQuiz();
                    return;
                case R.id.materialButton7:
                    this.option = 6;
                    startQuiz();
                    return;
                case R.id.materialButton8:
                    this.option = 7;
                    startQuiz();
                    return;
                case R.id.materialButton9:
                    this.option = 8;
                    startQuiz();
                    return;
                case R.id.materialButton10:
                    this.option = 9;
                    startQuiz();
                    return;
                default:
                    this.option = 0;
                    return;
            }
        }
        this.option = 0;

        if (this.langOpt == 1) {
            startActivity(new Intent(this,FirstChooserActivity.class));
        } else {
            startQuiz();
        }


    }

    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase));
    }

    private void startQuiz() {
        Intent intent = new Intent(this, QuizActivity.class);
        intent.putExtra("lang", this.langOpt);
        intent.putExtra("table", this.option);
        startActivity(intent);
    }

    private void changeLanguageButtonColors(int option) {
        if (option == 0) {
            this.langUzBtn.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.yello_light));
            this.langRuBtn.setBackgroundTintList(ContextCompat.getColorStateList(this, android.R.color.transparent));
            this.materialButton1.setText(R.string.type_1_uz);
            this.materialButton2.setText(R.string.type_2_uz);
            this.materialButton3.setText(R.string.type_3_uz);
            this.materialButton4.setText(R.string.type_4_uz);
            this.materialButton5.setText(R.string.type_5_uz);
            this.materialButton6.setText(R.string.type_6_uz);
            this.materialButton7.setText(R.string.type_7_uz);
            this.materialButton8.setText(R.string.type_8_uz);
            this.materialButton9.setText(R.string.type_9_uz);
            this.materialButton10.setText(R.string.type_10_uz);
            this.questionMark.setText(R.string.questions_uz);
            this.header1Txt.setText(R.string.header_text_1_uz);
            this.headerTxt2.setText(R.string.header_text_2_uz);
            this.bottom1txt.setText(R.string.bottom_text_1_uz);
            this.bottom2Txt.setText(R.string.bottom_text_2_uz);
            return;
        }
        this.langRuBtn.setBackgroundTintList(ContextCompat.getColorStateList(this, R.color.yello_light));
        this.langUzBtn.setBackgroundTintList(ContextCompat.getColorStateList(this, android.R.color.transparent));
        this.materialButton1.setText(R.string.type_1_ru);
        this.materialButton2.setText(R.string.type_2_ru);
        this.materialButton3.setText(R.string.type_3_ru);
        this.materialButton4.setText(R.string.type_4_ru);
        this.materialButton5.setText(R.string.type_5_ru);
        this.materialButton6.setText(R.string.type_6_ru);
        this.materialButton7.setText(R.string.type_7_ru);
        this.materialButton8.setText(R.string.type_8_ru);
        this.materialButton9.setText(R.string.type_9_ru);
        this.materialButton10.setText(R.string.type_10_ru);
        this.questionMark.setText(R.string.questions_ru);
        this.header1Txt.setText(R.string.header_text_1_ru);
        this.headerTxt2.setText(R.string.header_text_2_ru);
        this.bottom1txt.setText(R.string.bottom_text_1_ru);
        this.bottom2Txt.setText(R.string.bottom_text_2_ru);
    }
}

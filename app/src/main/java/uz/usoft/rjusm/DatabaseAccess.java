package uz.usoft.rjusm;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import uz.usoft.rjusm.models.Question;

public class DatabaseAccess {
    private static final String TABLE_TIP1_1_RU = "tip1_1_ru";
    private static final String TABLE_TIP1_2_RU = "tip1_2_ru";
    private static final String TABLE_TIP1_UZ = "tip1_uz";
    private static final String TABLE_TIP2_RU = "tip2_ru";
    private static final String TABLE_TIP2_UZ = "tip2_uz";
    private static final String TABLE_TIP3_RU = "tip3_ru";
    private static final String TABLE_TIP3_UZ = "tip3_uz";
    private static final String TABLE_TIP4_RU = "tip4_ru";
    private static final String TABLE_TIP4_UZ = "tip4_uz";
    private static final String TABLE_TIP5_RU = "tip5_ru";
    private static final String TABLE_TIP5_UZ = "tip5_uz";
    private static final String TABLE_TIP6_RU = "tip6_ru";
    private static final String TABLE_TIP6_UZ = "tip6_uz";
    private static final String TABLE_TIP7_RU = "tip7_ru";
    private static final String TABLE_TIP7_UZ = "tip7_uz";
    private static final String TABLE_TIP8_RU = "tip8_ru";
    private static final String TABLE_TIP8_UZ = "tip8_uz";
    private static final String TABLE_TIP9_RU = "tip9_ru";
    private static final String TABLE_TIP9_UZ = "tip9_uz";
    private static final String TABLE_TIP10_RU = "tip10_ru";
    private static final String TABLE_TIP10_UZ = "tip10_uz";

    private static DatabaseAccess instance;
    /* renamed from: c */
    Cursor c = null;
    private SQLiteDatabase db;
    private SQLiteOpenHelper openHelper;

    private DatabaseAccess(Context context) {
        this.openHelper = new DataBaseOpenHelper(context);
    }

    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    public void open() {
        this.db = this.openHelper.getWritableDatabase();
    }

    public void close() {
        SQLiteDatabase sQLiteDatabase = this.db;
        if (sQLiteDatabase != null) {
            sQLiteDatabase.close();
        }
    }

    public List<Question> getQuestions(int lang, int table) {
        List<Question> questionList = new ArrayList<>();
        StringBuilder query = new StringBuilder();
        query.append("SELECT  * FROM ");
        query.append(selectTable(lang, table));
        this.c = this.db.rawQuery(query.toString(), null);
        if (this.c.moveToFirst()) {
            do {
                Question question = new Question();
                question.setID(Integer.parseInt(this.c.getString(0)));
                question.setQUESTION(this.c.getString(1));
                question.setOPTA(this.c.getString(2));
                question.setOPTB(this.c.getString(3));
                question.setOPTC(this.c.getString(4));
                question.setOPTD(this.c.getString(5));
                question.setANSWER(this.c.getString(6));
                questionList.add(question);
            } while (this.c.moveToNext());
        }
        return questionList;
    }

    private String selectTable(int lang, int tableName) {
        if (lang == 0) {
            switch (tableName) {
                case 0:
                    return TABLE_TIP1_UZ;
                case 1:
                    return TABLE_TIP2_UZ;
                case 2:
                    return TABLE_TIP3_UZ;
                case 3:
                    return TABLE_TIP4_UZ;
                case 4:
                    return TABLE_TIP5_UZ;
                case 5:
                    return TABLE_TIP6_UZ;
                case 6:
                    return TABLE_TIP7_UZ;
                case 7:
                    return TABLE_TIP8_UZ;
                    case 8:
                    return TABLE_TIP9_UZ;
                    case 9:
                    return TABLE_TIP10_UZ;
                default:
                    return "";
            }
        }
        switch (tableName) {
            case 0:
                return "";
            case 1:
                return TABLE_TIP2_RU;
            case 2:
                return TABLE_TIP3_RU;
            case 3:
                return TABLE_TIP4_RU;
            case 4:
                return TABLE_TIP5_RU;
            case 5:
                return TABLE_TIP6_RU;
            case 6:
                return TABLE_TIP7_RU;
            case 7:
                return TABLE_TIP8_RU;
                case 8:
                return TABLE_TIP9_RU;
                case 9:
                return TABLE_TIP10_RU;
                case 10:
                return TABLE_TIP1_1_RU;
                case 11:
                return TABLE_TIP1_2_RU;

            default:
                return "";
        }
    }
}
